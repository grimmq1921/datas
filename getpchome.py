import requests
import json
import time

all_items = [] 
keyword = "apple"

pchome_keyword_url = "https://ecshweb.pchome.com.tw/search/v3.3/all/results?q=" + keyword + "&page=1&sort=sale/dc"
web = requests.get(url=pchome_keyword_url)
data = web.text
json_data = json.loads(data)
page_max = json_data['totalPage']


for i in range (page_max):
    page_num = i+1

    if page_num >2:
        break

    url = "https://ecshweb.pchome.com.tw/search/v3.3/all/results?q=" +keyword+ "&page=" + format(page_num) +"&sort=sale/dc"

    rs = requests.get(url=url)
    
    rs_data = rs.text
    try:
        page_data = json.loads(rs_data)
        item = page_data['prods']
     
        for pro in item:
            itme_name = pro['name']
            item_price = pro['price']

            good_item = f"商品名稱: {itme_name}  商品價格: {item_price}"
            all_items.append(good_item)

        time.sleep(1)

    except Exception as e:
        print(e)



# print(f"目前總共抓取{len(all_items)}件商品名稱")


for i in all_items:
    print(i)

